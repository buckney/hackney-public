package se.hackney.time.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FormatsTest {
	
	private Formats formats = null;
	
	@BeforeEach
	void setup() {
		formats = new Formats()
				.add( new Format( "yyyy-MM-dd", "[0-9]{4}-[0-9]{2}-[0-9]{2}", "Format A" ) )
				.add( new Format( "yyMMdd", "[0-9]{6}", "Format B" ) )
				.add( new Format( "yyyy-MM-dd HH:mm", "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}", "Format C" ) );
	}

	@Test
	void testMatch_match_formatA() {
		Format match = formats.match("2019-03-27");
		assertNotNull(match);
	}
	
	@Test
	void testMatch_match_formatB() {
		Format match = formats.match("190327");
		assertNotNull(match);
	}
	
	@Test
	void testMatch_noMatch() {
		Assertions.assertThrows( RuntimeException.class, () -> {
			formats.match("åäö");			
		} );
	}

	@Test
	void testGenerateMegaRegexString() {
		List< Format > formatsLocal = new ArrayList< Format >( Arrays.asList(
				new Format( "", "[0-9]+", "Test1" ),
				new Format( "", "[a-z]+", "Test2" )
		) );
		
		String expected = "([0-9]+)|([a-z]+)";
		String actual = new Formats().generateMegaRegexString( formatsLocal );
		
		assertEquals( expected, actual );
	}
	
	@Test
	void transformDate() {
		Date initial = formats.asDate( "2015-02-07 12:13" );
		Date expected = formats.asDate( "2015-05-05 17:30" );
		Date actual = formats.transform( initial, "3M -2d 17[H] 17m");
		
		assertEquals( expected, actual );
	}
	
	@Test
	void transformString() {
		String expected = "2013-01-01 17:05";
		String actual = formats.transform( "2013-03-03 15:30", "-2M -2d 17[H] -25m");
		assertEquals(expected, actual);
	}

}
