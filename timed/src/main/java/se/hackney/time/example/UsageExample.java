package se.hackney.time.example;

import se.hackney.time.api.Format;
import se.hackney.time.api.Formats;

public class UsageExample {
	
	private static final Formats formats = new Formats()
			.add( new Format( "yyyy-MM-dd", "[0-9]{4}-[0-9]{2}-[0-9]{2}", "Year-month-day" ) )
			.add( new Format( "yyyyMMdd", "[0-9]{6}", "Short" )
	);
	
	public static Formats getApprovedFormats() {
		return formats;
	}

}
