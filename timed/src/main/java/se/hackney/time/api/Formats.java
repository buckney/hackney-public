package se.hackney.time.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Formats {
	private static final Pattern transformPattern = Pattern.compile( "([+-]?)([0-9]+)([yMdHmsS]+)|([0-9]+)\\[([yMdHmsS]+)\\]" );
	
	private List< Format > formats = new ArrayList< Format >();
	private Pattern pattern = null;
	
	public Formats add( Format format ) {
		pattern = null;
		formats.add( format );
		return this;
	}
	
	
	public Date asDate( String input ) {
		
		Format format = match( input );
		return format.asDate( input );
		
	}
	
	
	public Format match( String input ) {
		
		if( pattern == null ) {
			pattern = Pattern.compile( generateMegaRegexString( formats ) );
		}
		
		Matcher matcher = pattern.matcher( input );
		
		if( !matcher.matches() ) {
			throw new RuntimeException( "FormatException");
		};
		
		for( int index = 1; index <= formats.size(); index++ ) {
			if( matcher.group(index) != null ) {
				return formats.get( index - 1 );
			}
		}
		
		throw new RuntimeException ( "WtfException" );

	}
	
	
	public String transform( String input, String allTransformations ) {
		
		Format format = match( input );
		Date inputDate = format.asDate( input );
		Date transformedDate = transform( inputDate, allTransformations );
		
		return format.asString( transformedDate );
		
	}
	
	
	public Date transform( Date input, String allTransformations ) {
		
		if ( allTransformations == null ) {
			throw new RuntimeException( "NullTransformationException" );
		}
		
		
		List< String > transformations = new ArrayList< String >( Arrays.asList( allTransformations.split(" ") ) );
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis( input.getTime() );
		
		for( String transformation : transformations ) {
			
			Matcher matcher = transformPattern.matcher( transformation );

			if ( matcher.matches() ) {

				// Is absolute transformation
				if( matcher.group( 4 ) != null ) {
					Integer value = Integer.parseInt( matcher.group( 4 ) );
					String selector = matcher.group( 5 );
					
					switch( selector ) {
						case "y" : {
							calendar.set( Calendar.YEAR, value );
							break;
						}
						
						case "M" : {
							calendar.set( Calendar.MONTH, value );
							break;
						}
						
						case "d" : {
							calendar.set( Calendar.DAY_OF_MONTH, value );
							break;
						}
						
						case "H" : {
							calendar.set( Calendar.HOUR_OF_DAY, value );
							break;
						}
						
						case "m" : {
							calendar.set( Calendar.MINUTE, value );
							break;
						}
						
						case "s" : {
							calendar.set( Calendar.SECOND, value );
							break;
						}
						
						case "S" : {
							calendar.set( Calendar.MILLISECOND, value );
							break;
						}
					}
					
				} else {
					// If relative transformation
					
					Integer value = null;
					
					if( "-".equals( matcher.group( 1 ) ) ) {
						value = Integer.parseInt( "-" + matcher.group( 2 ) );						
					} else {
						value = Integer.parseInt( matcher.group( 2 ) );
					}
					
					
					String selector = matcher.group( 3 );
					
					switch( selector ) {
					
						case "y" : {
							calendar.add( Calendar.YEAR, value );
							break;
						}
						
						case "M" : {
							calendar.add( Calendar.MONTH, value );
							break;
						}
						
						case "d" : {
							calendar.add( Calendar.DAY_OF_YEAR, value );
							break;
						}
						
						case "H" : {
							calendar.add( Calendar.HOUR_OF_DAY, value );
							break;
						}
						
						case "m" : {
							calendar.add( Calendar.MINUTE, value );
							break;
						}
						
						case "s" : {
							calendar.add( Calendar.SECOND, value );
							break;
						}
						
						case "S" : {
							calendar.add( Calendar.MILLISECOND, value );
							break;
						}
					
					}
				}
				
			} else {
				throw new RuntimeException( "FaultyTransformationException | " + transformation );
			}
			
		}
		
		return calendar.getTime();
	}

	
	protected String generateMegaRegexString( List< Format > formats ) {
		
		boolean notFirst = false;
		StringBuilder megaString = new StringBuilder();
		
		for( Format format : formats ) {
			
			if( notFirst ) {
				megaString.append( "|");
			}
			
			megaString.append( "(" + format.pattern().pattern() + ")" );
			notFirst = true;
		}
		
		return megaString.toString();
		
	}
}
