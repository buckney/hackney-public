package se.hackney.time.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Format {
	private String[] names;
	private SimpleDateFormat format;
	private Pattern pattern;
	
	public Format( String format, String regex, String... names ) {
		this.names = names;
		this.format = new SimpleDateFormat( format );
		this.pattern = Pattern.compile( regex );
	}
	
	public boolean valid( String time ) {
		Matcher matcher = pattern.matcher( time );
		return matcher.matches();
	}
	
	public String asString( Object object ) {
		return format.format( object );
	}
	
	public Date asDate( String source ) {
		try {
			return format.parse( source );
			
		} catch (ParseException e) {
			return null;
		}
	}
	
	protected Pattern pattern() {
		return pattern;
	}
	
}
